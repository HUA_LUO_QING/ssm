package com.hyl.exception;

import com.hyl.controller.Code;

/**
 * @author hyl
 * @version 1.0
 * @date 2023/1/10-17:52
 * 自定义异常处理器，用于封装异常信息，对异常进行分类
 */


public class SystemException extends RuntimeException{
    private Code code;

    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    public SystemException(Code code, String message) {
        super(message);
        this.code = code;
    }

    public SystemException(Code code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

}

