package com.hyl.config;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * @author hyl
 * @version 1.0
 * @date 2023/1/10-14:33
 */
@Component
public class MybatisConfig {
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource){
        SqlSessionFactoryBean sqlSession=new SqlSessionFactoryBean();
        //设置模型类的别名扫描
        sqlSession.setTypeAliasesPackage("com.hyl.pojo");
        //设置数据源
        sqlSession.setDataSource(dataSource);
        return sqlSession;
    }
    @Bean
    public MapperScannerConfigurer mappers(){
        MapperScannerConfigurer msc = new MapperScannerConfigurer();
        msc.setBasePackage("com.hyl.mapper");
        return msc;
    }
}
