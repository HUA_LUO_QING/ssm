package com.hyl.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author hyl
 * @version 1.0
 * @date 2023/1/9-14:05
 */
@Configuration
@ComponentScan({"com.hyl.controller","com.hyl.config"})
@EnableWebMvc
public class SpringMvcConfig {

}
