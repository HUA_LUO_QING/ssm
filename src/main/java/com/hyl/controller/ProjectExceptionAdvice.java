package com.hyl.controller;

import com.hyl.exception.BusinessException;
import com.hyl.exception.SystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author hyl
 * @version 1.0
 * @date 2023/1/10-18:50
 * @RestControllerAdvice 用于标识当前类为REST风格对应的异常处理器
 */


@RestControllerAdvice
public class ProjectExceptionAdvice {
    /**
     * @ExceptionHandler 用于设置当前处理器类对应的异常类型
     * @param ex
     * @return
     */
    @ExceptionHandler(SystemException.class)
    public Result doSystemException(SystemException ex){
        //记录日志
        //发送消息给运维
        //发送邮件给开发人员,ex对象发送给开发人员
        return new Result(ex.getCode(),null,ex.getMessage());
    }

    /**
     * 自定义的业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    public Result doBusinessException(BusinessException ex){
        return new Result(ex.getCode(),null,ex.getMessage());
    }

    /**
     * 除了自定义的异常处理器，保留对Exception类型的异常处理，用于处理非预期的异常
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public Result doOtherException(Exception ex){
        //记录日志
        //发送消息给运维
        //发送邮件给开发人员,ex对象发送给开发人员
        return new Result(Code.SYSTEM_UN_KNOW_ERR,null,"系统繁忙，请稍后再试！");
    }
}

