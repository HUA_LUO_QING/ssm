package com.hyl.controller;

/**
 * @author hyl
 * @version 1.0
 * @date 2023/1/10-17:22
 * 状态码（枚举类）
 */

public enum Code {

    //保存成功
    SAVE_OK(20011),
    //删除成功
    DELETE_OK(20021),
    //更新成功
    UPDATE_OK(20031),
    //查询成功
    GET_OK(20041),
    //保存失败
    SAVE_ERR(20010),
    //删除失败
    DELETE_ERR(20020),
    //更新失败
    UPDATE_ERR(20030),
    //查询失败
    GET_ERR(20040),
    //系统异常
    SYSTEM_ERR(50001),
    //超时异常
    SYSTEM_TIMEOUT_ERR(50002),
    //未知异常
    SYSTEM_UN_KNOW_ERR(59999),
    //业务异常
    BUSINESS_ERR(60002);


    private Integer code;

    private Code(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
