package com.hyl.service;

import com.hyl.pojo.Book;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author hyl
 * @version 1.0
 * @date 2023/1/10-14:44
 */

@Transactional
public interface BookService {
    /**
     * 保存
     * @param book
     * @return
     */
    public boolean save(Book book);

    /**
     * 修改
     * @param book
     * @return
     */
    public boolean update(Book book);

    /**
     * 按id删除
     * @param id
     * @return
     */
    public boolean delete(Integer id);

    /**
     * 依据id查询
     * @param id
     * @return
     */
    public Book getById(Integer id);

    /**
     * 按name查询
     * @param input
     * @return
     */
    public List<Book> getByName(String input);

    /**
     * 查询全部
     * @return
     */
    public List<Book> getAll();
}

