import com.hyl.config.SpringConfig;
import com.hyl.pojo.Book;
import com.hyl.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author hyl
 * @version 1.0
 * @date 2023/1/10-15:12
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class SpringTest {
    @Autowired
    private BookService bookService;

    @Test
    public void test01(){
        Book book = bookService.getById(1);
        System.out.println(book);
    }


}
